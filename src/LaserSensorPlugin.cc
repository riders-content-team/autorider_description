#include <rosrider_description/LaserSensorPlugin.hh>

using namespace gazebo;
// Register this plugin with the simulator
GZ_REGISTER_SENSOR_PLUGIN(LaserSensorPlugin)

// Constructor
LaserSensorPlugin::LaserSensorPlugin() : SensorPlugin()
{
}

// Destructor
LaserSensorPlugin::~LaserSensorPlugin()
{
}

void LaserSensorPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
  this->sensor = _sensor;
  this->world = physics::get_world(this->sensor->WorldName());

  // Check sensor
  if (!_sensor)
    gzerr << "Invalid sensor pointer.\n";

  this->parentSensor =
    std::dynamic_pointer_cast<sensors::RaySensor>(_sensor);

  if (!this->parentSensor)
    gzthrow("RayPlugin requires a Ray Sensor as its parent");

  this->rangeVector.resize(this->parentSensor->RayCount());

  // Get robot namespace
  if (!_sdf->HasElement("robotNamespace"))
  {
    this->robot_namespace_ = "robot";
  } else {
    this->robot_namespace_ = _sdf->Get<std::string>("robotNamespace");
  }

  // Get frame name
  if (!_sdf->HasElement("frameName"))
  {
    this->frame_name_ = "world";
  } else {
    this->frame_name_ = _sdf->Get<std::string>("frameName");
  }

  // Get topic name
  // If empty generate from frame name
  if (!_sdf->HasElement("sensorTopicName"))
  {
    this->topic_name.append(frame_name_);
    this->topic_name.append("/range");
  } else {
    this->topic_name = _sdf->Get<std::string>("sensorTopicName");
  }

  // Get service name
  // If empty generate from frame name
  if (!_sdf->HasElement("sensorServiceName"))
  {
    this->service_name.append(frame_name_);
    this->service_name.append("/get_range");

  } else {
    this->service_name = _sdf->Get<std::string>("sensorServiceName");
  }


  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized())
  {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, robot_namespace_,
      ros::init_options::NoSigintHandler);
  }

  // Create our ROS node. This acts in a similar manner to
  // the Gazebo node
  this->rosNode.reset(new ros::NodeHandle(robot_namespace_));
  // laser Publisher
  this->laser_sensor_publisher = this->rosNode->advertise<std_msgs::Float32>(this->topic_name, 100);

  // Service Options
  ros::AdvertiseServiceOptions laser_sensor_options =

  ros::AdvertiseServiceOptions::create<rosrider_description::LaserSensor>(
    this->service_name,
      boost::bind(&LaserSensorPlugin::RangeCallback, this, _1, _2),
        ros::VoidConstPtr(), NULL);

  // Advertise Service
  this->laser_service = this->rosNode->advertiseService(laser_sensor_options);


  this->newLaserScansConnection =
    this->parentSensor->LaserShape()->ConnectNewLaserScans(
      std::bind(&LaserSensorPlugin::OnNewLaserScans, this));


  // This must be called
  this->parentSensor->SetActive(true);
}

void LaserSensorPlugin::OnNewLaserScans()
{
  this->parentSensor->Ranges(this->rangeVector);
  this->distance = this->rangeVector[0];

  this->range_msg.data = this->distance;

  this->laser_sensor_publisher.publish(range_msg);
  //std::cout << this->distance << std::endl;

  ros::spinOnce();
}

bool LaserSensorPlugin::RangeCallback(rosrider_description::LaserSensor::Request &req, rosrider_description::LaserSensor::Response &res)
{
  double sim_time = this->world->SimTime().Double();
  // Header
  res.seq = this->sequence;
  this->sequence++;

  res.stamp = ros::Time(sim_time);
  res.frame_id = this->frame_name_;

  // Data
  //this->parentSensor->Ranges(this->rangeVector);
  //this->distance = this->rangeVector[0];

  res.data = this->distance;

  return true;
}
